package us.empiric;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Fecha {
	
	private static final Logger logger = LoggerFactory.getLogger(Fecha.class);
	
	public static void main(String[] args) {
		
		logger.debug("[MAIN] Fecha actual : {}", getCurrentDate());
		System.out.println(getCurrentDate());
	}
	
	private static Date getCurrentDate(){
		return new Date();
	}
	
}
